# -*- coding: utf-8 -*-

import numpy as np
import os
import h5py

def read_stock_list(db_dir):
    data_dir = os.path.join(db_dir, 'data')
    list_file_dir = os.path.join(data_dir, 'stock_list.h5')

    assert os.path.exists(list_file_dir), 'List file not found'

    file = h5py.File(list_file_dir, 'r')
    stock_list = file['stock_list']
    file.close()
    return stock_list

def read_stock_k_data(db_dir, stock_id):
    data_dir = os.path.join(db_dir, 'data')
    stock_file_dir = os.path.join(data_dir, stock_id + '.h5')

    assert os.path.exists(stock_file_dir), 'Stock file not found'

    file = h5py.File(stock_file_dir, 'r')

    stock_k_data = file['k_data'][:]
    return stock_k_data

def __process_k_data(stock_k_data):
    '''把k线数据变成浮点数'''

    return

if __name__ == '__main__':
    print read_stock_k_data(db_dir = 'D:\Quant', stock_id = '000962')
