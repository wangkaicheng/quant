# -*- coding: utf-8 -*-

import tushare as ts
import h5py
import numpy as np
import pandas as pd
import os
import time

def update_stock_list(db_dir):
    '''更新股票列表'''
    data_dir = os.path.join(db_dir, 'data')
    list_file_dir = os.path.join(data_dir, 'stock_list.h5')
    if not os.path.exists(data_dir):
        os.mkdir(data_dir)
    stock_basic = ts.get_stock_basics()
    stock_index = list(stock_basic.index)
    if not os.path.exists(list_file_dir):
        print 'Stock list file not found, creat a new one'
        file = h5py.File(list_file_dir, 'w')
    else:
        file = h5py.File(list_file_dir, 'r+')
        old_stock_index = file.pop('stock_list')
        print 'Stock list update %d stocks' % (len(stock_index)- len(old_stock_index))
    file.create_dataset('stock_list', data=stock_index)
    file.close()
    return

def updata_stock_k_data(db_dir):
    '''根据数据库的股票列表更新股票数据'''
    data_dir = os.path.join(db_dir, 'data')
    trade_date = ts.get_k_data('000001', start = '2015-01-01', index=True)['date']
    wrong_data_list = []
    if not os.path.exists(data_dir):
        os.mkdir(data_dir)
    list_file_dir = os.path.join(data_dir, 'stock_list.h5')
    if not os.path.exists(list_file_dir):
        update_stock_list(db_dir)
    file = h5py.File(list_file_dir, 'r')
    stock_list = file['stock_list'][:]
    file.close()
    print 'Stock list file has been read. Begin update k data of %d stocks' % (len(stock_list))
    for stock_id in stock_list:
        _get_ts_k_data(data_dir, stock_id, wrong_data_list, trade_date)

    print wrong_data_list
    return

def _get_ts_k_data(data_dir, stock_id, wrong_data_list, trade_date, start = '2015-01-01'):
    '''更新股票数据，存储在股票数据库'''
    stock_file_dir = os.path.join(data_dir, stock_id + '.h5')
    #获取最近的交易日
    nearst_trade_day = str(trade_date[len(trade_date)-1])
    #若文件不存在，直接读取ts信息存入本地文件
    if not os.path.exists(stock_file_dir):

        k_data = _process_k_data(ts.get_k_data(stock_id, start=start, retry_count=10, pause=0.01))
        stock_k_data, wrong_data_flag = _process_ts_data(k_data, trade_date)
        if wrong_data_flag:
            print '%s date data wrong' % (stock_id)
            wrong_data_list.append(stock_id)
        else:
            file = h5py.File(stock_file_dir, 'w')

            file.create_dataset('k_data', data = stock_k_data)
            file.close()
    else:#若文件存在，读取验证是否最后的数据是今日的，若是则不更新
        file = h5py.File(stock_file_dir, 'r+')
        #检查数据是否为空
        if len(file['k_data'][0])==0:
            k_data = _process_k_data(ts.get_k_data(stock_id, start=start, retry_count=10, pause=0.01))
            stock_k_data, wrong_data_flag = _process_ts_data(k_data, trade_date)
            if wrong_data_flag:
                print '%s date data wrong' % (stock_id)
                wrong_data_list.append(stock_id)
            else:
                file.pop('k_data')
                file.create_dataset('k_data', data=stock_k_data)
        #检查数据是否是最新交易日
        elif file['k_data'][0][-1] != nearst_trade_day:
            k_data = _process_k_data(ts.get_k_data(stock_id, start=start, retry_count=10, pause=0.01))
            stock_k_data, wrong_data_flag = _process_ts_data(k_data, trade_date)
            if wrong_data_flag:
                print '%s date data wrong' % (stock_id)
                wrong_data_list.append(stock_id)
            else:
                file.pop('k_data')
                file.create_dataset('k_data', data=stock_k_data)
        else:
            print stock_id+' data is new'
        file.close()

    return

def _process_k_data(k_data):
    return k_data

def _process_h_data(h_data):
    h_data = h_data[::-1]
    return h_data

def _process_hist_data(hist_data):
    hist_data = hist_data[::-1]
    return hist_data

def _process_ts_data(k_data, trade_date, wrong_data_flag = False):
    '''把数据处理成(6,N)的list，每列分别是date, open, close, high, low, volume'''
    stock_k_data = [[], [], [], [], [], []]
    if len(k_data) == 0:
        return stock_k_data, wrong_data_flag
    if _date_cheak(k_data['date'], trade_date):
        for i in xrange(len(k_data)):
            stock_k_data[0].append(str(k_data['date'][i]))
            stock_k_data[1].append(k_data['open'][i])
            stock_k_data[2].append(k_data['close'][i])
            stock_k_data[3].append(k_data['high'][i])
            stock_k_data[4].append(k_data['low'][i])
            stock_k_data[5].append(k_data['volume'][i])

    else:
        wrong_data_flag = True
    return stock_k_data, wrong_data_flag

def _date_cheak(k_date, trade_date):
    if len(k_date) > len(trade_date):
        return False
    else:
        i, j = 0, 0
        while i<len(k_date) and j<len(trade_date):
            if k_date[i] == trade_date[j]:
                i += 1
                j += 1
            else:
                j += 1
        return i == len(k_date)



if __name__ == '__main__':
    #update_stock_list('D:\Quant')
    updata_stock_k_data('D:\Quant')


